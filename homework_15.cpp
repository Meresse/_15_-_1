#include <iostream>
using namespace std;

void f(int n) { 

	cout << 0 << " ";

	for (int i = 2 - n % 2; i <= n; i += 2) {

		cout << i << " ";

	}
}

int main()
{
	int N;
	cout << "N = ";
	cin >> N;

	f(N);
}